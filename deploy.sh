#!/bin/bash

# Abort the script if a command returns an error.
set -e

if [[ "$1" != "--no-build" ]]; then
  pnpm build
fi


if [ -f .env.deploy ]; then
  # Export variables from .env.deploy file.
  set -o allexport
  source .env.deploy
  set +o allexport
fi


# Copy the content to a temporary directory.
rsync -avz --delete --quiet \
  -e ssh ./public/ ./deploy/

# Set version.
VERSION=$(date --iso-8601=seconds)
echo $VERSION > ./deploy/version.txt
sed -i "s/{{VERSION}}/$VERSION/g" ./deploy/index.html

# Generate the configuration file.
cat <<ENV > ./deploy/config.php
<?php
// Generated during deployment.
\$config = [
  'SQLITE_DATABASE' => '$SQLITE_DATABASE',
  'FILES_DIR' => '$FILES_DIR',
  'MAX_ALLOWED_FILE_SIZE' => '$MAX_ALLOWED_FILE_SIZE',
  'VERSION' => '$VERSION',
];
ENV


# Transfer the content to the remote directory.
rsync -avz --delete -e ssh ./deploy/ "$USERNAME@$REMOTE_HOST:$REMOTE_DIR"
