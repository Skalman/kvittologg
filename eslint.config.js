// @ts-check

import { fixupConfigRules } from "@eslint/compat";
import { FlatCompat } from "@eslint/eslintrc";
import eslint from "@eslint/js";
// @ts-expect-error: Missing types.
import react from "eslint-plugin-react/configs/recommended.js";
// @ts-expect-error: Missing types.
import reactHooks from "eslint-plugin-react-hooks";
import simpleImportSort from "eslint-plugin-simple-import-sort";
// @ts-expect-error: Missing types.
import tailwind from "eslint-plugin-tailwindcss";
import globals from "globals";
import tseslint from "typescript-eslint";

const flatCompat = new FlatCompat();

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.strictTypeChecked,
  ...tseslint.configs.stylisticTypeChecked,
  ...tailwind.configs["flat/recommended"],
  // @ts-expect-error The types mismatch.
  ...fixupConfigRules(react),
  // @ts-expect-error The types mismatch.
  ...fixupConfigRules(flatCompat.config(reactHooks.configs.recommended)),
  {
    plugins: {
      "simple-import-sort": simpleImportSort,
    },
    rules: {
      "simple-import-sort/imports": "error",
      "simple-import-sort/exports": "error",
    },
  },
  {
    languageOptions: {
      parserOptions: {
        projectService: true,
        tsconfigRootDir: import.meta.url.replace(/^\/[^/]+$/, ""),
      },
    },
  },
  {
    files: ["*.js", "*.cjs"],
    extends: [tseslint.configs.disableTypeChecked],
    languageOptions: {
      globals: globals.node,
    },
  },
  {
    ignores: ["public/dist/*", "deploy/*", "node_modules", "data"],
  },
  {
    settings: {
      react: { version: "19" },
    },
  },
);
