import daisyui from "daisyui";
import type { Config } from "tailwindcss";

export default {
  content: ["./src/**/*", "./index.html"],
  theme: {
    extend: {
      animation: {
        appear: "appear 0.15s ease-in-out forwards",
      },
      keyframes: {
        appear: {
          "0%": { transform: "scale(0.9)", opacity: "0" },
          "100%": { transform: "", opacity: "1" },
        },
      },
    },
  },
  plugins: [daisyui],
  daisyui: {
    themes: ["light", "dark"],
  },
} satisfies Config;
