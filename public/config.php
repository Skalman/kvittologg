<?php

// This file will be overwritten during deployment.

chdir(__DIR__ . '/..');

$config = parse_ini_file('.env');
$config['VERSION'] = getVersion();

function getVersion()
{
  $filesToCheck = ['dist/index.js', 'dist/index.css', 'index.html', 'api.php'];
  $latestModification = max(
    array_map(
      fn($file) => filemtime("public/$file"),
      $filesToCheck
    )
  );
  $version = date('c', $latestModification);
  return $version;
}