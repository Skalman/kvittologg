<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
set_error_handler('errorHandler');

$isMainScript = !debug_backtrace();
require __DIR__ . '/config.php';

function errorHandler($errno, $errstr, $errfile, $errline)
{
  // error was suppressed with the @-operator
  if (0 === error_reporting()) {
    return false;
  }

  throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}

// Polyfill for PHP 7.
if (!function_exists('str_starts_with')) {
  function str_starts_with($haystack, $needle)
  {
    return substr($haystack, 0, strlen($needle)) == $needle;
  }
}

function runApi(array $actions)
{
  if (isset($_GET['action'], $actions[$_GET['action']])) {
    $actions[$_GET['action']]();
  } else {
    notFound();
  }
}

final class Config
{
  public string $version;
  public string $sqliteDatabase;
  public string $filesDir;
  public int $maxAllowedFileSize;

  public function __construct(array $config)
  {
    if (isset($config['SQLITE_DATABASE'])) {
      $this->sqliteDatabase = $config['SQLITE_DATABASE'];
    } else {
      throw new Error('Missing config SQLITE_DATABASE');
    }

    if (isset($config['FILES_DIR'])) {
      $this->filesDir = $config['FILES_DIR'];
    } else {
      throw new Error('Missing config FILES_DIR');
    }

    if (isset($config['MAX_ALLOWED_FILE_SIZE'])) {
      $this->maxAllowedFileSize = $config['MAX_ALLOWED_FILE_SIZE'];
    } else {
      throw new Error('Missing config MAX_ALLOWED_FILE_SIZE');
    }

    if (isset($config['VERSION'])) {
      $this->version = $config['VERSION'];
    }
  }
}

function getConfig(): Config
{
  static $config = null;

  if ($config === null) {
    $config = new Config($GLOBALS['config']);
  }

  return $config;
}

function getDb()
{
  static $db = null;

  if ($db === null) {
    $config = getConfig();
    $db = new PDO($config->sqliteDatabase);
    initDb($db);
  }

  return $db;
}

function initDb(PDO $db)
{
  $result = $db->exec(
    'CREATE TABLE IF NOT EXISTS user (
      user_id TEXT PRIMARY KEY,
      username TEXT NOT NULL,
      username_display TEXT NOT NULL,
      password TEXT NOT NULL,
      created_at TEXT NOT NULL
    );
    CREATE UNIQUE INDEX IF NOT EXISTS idx_user_username ON user (username);

    CREATE TABLE IF NOT EXISTS session (
      session_key TEXT PRIMARY KEY,
      user_id TEXT NOT NULL,
      created_at TEXT NOT NULL,
      FOREIGN KEY (user_id) REFERENCES user (user_id)
    );

    CREATE UNIQUE INDEX IF NOT EXISTS idx_session_user_id_session_key ON session (user_id, session_key);

    CREATE TABLE IF NOT EXISTS receipt (
      receipt_id TEXT PRIMARY KEY,
      user_id TEXT NOT NULL,
      created_at TEXT NOT NULL,
      receipt_date TEXT NOT NULL,
      description TEXT,
      location_coords TEXT,
      location_text TEXT,
      FOREIGN KEY (user_id) REFERENCES user (user_id)
    );
    CREATE INDEX IF NOT EXISTS idx_receipt_user_date ON receipt (user_id, receipt_date);

    CREATE TABLE IF NOT EXISTS file (
      file_id TEXT PRIMARY KEY,
      hash TEXT NOT NULL,
      extension TEXT NOT NULL,
      size INTEGER NOT NULL,
      created_at TEXT NOT NULL
    );
    CREATE INDEX IF NOT EXISTS idx_file_hash ON file (hash);

    CREATE TABLE IF NOT EXISTS receipt_file (
      receipt_id TEXT,
      file_id TEXT,
      PRIMARY KEY (receipt_id, file_id),
      FOREIGN KEY (receipt_id) REFERENCES receipt (receipt_id),
      FOREIGN KEY (file_id) REFERENCES file (file_id)
    );

    -- TODO delete receipt_file, move file>receipt_file
    '
  );
  if ($result === false) {
    internalServerError($db->errorInfo());
  }
}

function executeStatement(string $sql, array $params = []): PDOStatement
{
  $db = getDb();
  $stmt = $db->prepare($sql);
  if ($db->errorCode() !== '00000') {
    internalServerError([
      'error' => 'database-error',
      'errorInfo' => $db->errorInfo(),
    ]);
  }

  $stmt->execute($params);

  if ($stmt->errorCode() !== '00000') {
    internalServerError([
      'error' => 'database-error',
      'errorInfo' => $stmt->errorInfo(),
    ]);
  }

  return $stmt;
}

function fetchSingleRow(string $sql, array $params = []): ?array
{
  $stmt = executeStatement($sql, $params);
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
  if ($result === false) {
    return null;
  }

  return $result;
}

function fetchAllRows(string $sql, array $params = []): array
{
  $stmt = executeStatement($sql, $params);
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function executeNonQuery(string $sql, array $params = []): int
{
  $stmt = executeStatement($sql, $params);
  return $stmt->rowCount();
}

function getBody()
{
  return @json_decode(@file_get_contents('php://input'), true);
}

// Source: https://stackoverflow.com/a/15875555
function uuidv4()
{
  $data = random_bytes(16);

  $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
  $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

  return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function getFileDir($fileHash)
{
  $config = getConfig();
  $subDirs = substr($fileHash, 0, 2) . '/' . substr($fileHash, 2, 2);
  return "$config->filesDir/$subDirs";
}

function getFileUrl($fileId)
{
  return removeQueryString($_SERVER['REQUEST_URI']) . "?action=getFile&fileId=$fileId";
}

function notFound(): never
{
  header('HTTP/1.1 404 Not Found');
  output('Not found');
  exit;
}

function badRequest($obj): never
{
  header('HTTP/1.1 400 Bad Request');
  output($obj);
  exit;
}

function unauthorized($obj): never
{
  header('HTTP/1.1 401 Unauthorized');
  output($obj);
  exit;
}

function internalServerError($obj): never
{
  header('HTTP/1.1 500 Internal Server Error');
  output($obj);
  exit;
}

function output($obj)
{
  if (is_string($obj)) {
    $obj = ['message' => $obj];
  }

  header('Content-Type: application/json');
  echo json_encode($obj);
}

function getIsoDate(string $datetime)
{
  return (new DateTime($datetime, new DateTimeZone('UTC')))->format('c');
}

function removeQueryString(string $url)
{
  return preg_replace('/\?.*$/', '', $url);
}

function setSession(?string $sessionKey)
{
  setcookie('kvittologg-session', $sessionKey ?? '', [
    'expires' =>
      $sessionKey === null
      ? 0
      : time() + 30 * 24 * 60 * 60,
    'path' => removeQueryString($_SERVER['REQUEST_URI']),
    'secure' => true,
    'httponly' => true,
    'samesite' => 'Strict',
  ]);
}

function getSessionKey(): ?string
{
  if (isset($_COOKIE['kvittologg-session'])) {
    return $_COOKIE['kvittologg-session'];
  }

  return null;
}

function getSessionUserId(): string
{
  if (isset($_COOKIE['kvittologg-session'])) {
    $user = fetchSingleRow(
      'SELECT user_id AS userId
      FROM session
      WHERE session_key = :session_key',
      [':session_key' => $_COOKIE['kvittologg-session']]
    );

    if ($user) {
      return $user['userId'];
    }

    setSession(null);
  }

  unauthorized(['error' => 'login-required']);
}

function isAllowedDataUrl(string $dataUrl, int $maxBytes): bool
{
  $requiredStart = 'data:image/jpeg;base64,';
  return str_starts_with($dataUrl, $requiredStart) && strlen($dataUrl) <= strlen($requiredStart) + $maxBytes / 3 * 4;
}

function parseDataUrl(string $dataUrl): array
{
  $matches = null;
  if (!preg_match('/^data:([^;]+);base64,/', $dataUrl, $matches)) {
    internalServerError(['error' => 'Unexpeted file data']);
  }

  $extension = mapMimeTypeToExtension($matches[1]);
  if (!$extension) {
    internalServerError(['error' => "Unsupported mime type $matches[1]"]);
  }

  $data = base64_decode(substr($dataUrl, strlen($matches[0])));

  return [
    'extension' => $extension,
    'data' => $data,
  ];
}

function mapMimeTypeToExtension(string $mimeType): ?string
{
  switch ($mimeType) {
    case 'image/jpeg':
      return 'jpeg';
  }
}

$actions = [
  'createUser' => function () {
    $body = getBody();
    empty ($body['username']) && badRequest('Username is required');
    empty ($body['password']) && badRequest('Password is required');

    if (strlen($body['password']) < 6) {
      badRequest('Password must be at least 6 chars');
    }

    $usernameLc = mb_strtolower($body['username']);
    $passwordHash = password_hash($body['password'], PASSWORD_DEFAULT);
    $userId = uuidv4();
    $sessionKey = uuidv4();

    try {
      executeNonQuery(
        'INSERT INTO user (user_id, username, username_display, password, created_at)
        VALUES (:user_id, :username, :username_display, :password, :created_at);',
        [
          'user_id' => $userId,
          'username' => $usernameLc,
          'username_display' => $body['username'],
          'password' => $passwordHash,
          'created_at' => getIsoDate('now'),
        ]
      );

      executeNonQuery(
        'INSERT INTO session (session_key, user_id, created_at)
        VALUES (:session_key, :user_id, :created_at);',
        [
          'session_key' => $sessionKey,
          'user_id' => $userId,
          'created_at' => getIsoDate('now'),
        ]
      );

      setSession($sessionKey);
      output([
        'userId' => $userId,
        'sessionKey' => $sessionKey,
      ]);
    } catch (PDOException $e) {
      if ($e->getCode() === '23000') {
        // Duplicate entry error (SQLite error code 23000)
        badRequest(['message' => 'Username is already in use.']);
      } else {
        // Other PDO exception
        internalServerError($e->getMessage());
      }
    }
  },

  'changePassword' => function () {
    $userId = getSessionUserId();
    $body = getBody();
    empty ($body['password']) && badRequest('Missing prop password');
    empty ($body['oldPassword']) && badRequest('Missing prop oldPassword');

    $user = fetchSingleRow(
      'SELECT password
      FROM user
      WHERE user_id = :user_id',
      ['user_id' => $userId]
    );

    if (!$user) {
      unauthorized('Session is invalid');
    }

    if (!password_verify($body['oldPassword'], $user['password'])) {
      badRequest('Old password is incorrect');
    }

    $newPasswordHash = password_hash($body['password'], PASSWORD_DEFAULT);

    $updateResult = executeNonQuery(
      'UPDATE user
      SET password = :new_password
      WHERE user_id = :user_id;',
      [
        'new_password' => $newPasswordHash,
        'user_id' => $userId,
      ]
    );

    if (!$updateResult) {
      internalServerError('Failed to update password');
    }

    output(['status' => 'success']);
  },

  'login' => function () {
    $body = getBody();
    empty ($body['username']) && badRequest('Missing prop username');
    empty ($body['password']) && badRequest('Missing prop password');

    $usernameLc = mb_strtolower($body['username']);

    $user = fetchSingleRow(
      'SELECT
        user_id AS userId,
        password
      FROM user
      WHERE username = :username',
      ['username' => $usernameLc]
    );

    if (!$user) {
      unauthorized(['message' => 'Invalid username']);
    }

    if (!password_verify($body['password'], $user['password'])) {
      unauthorized(['message' => 'Invalid password']);
    }

    if (password_needs_rehash($user['password'], PASSWORD_DEFAULT)) {
      $newPasswordHash = password_hash($body['password'], PASSWORD_DEFAULT);
      executeNonQuery(
        'UPDATE user
        SET password = :new_password
        WHERE username = :username',
        [
          'username' => $usernameLc,
          'new_password' => $newPasswordHash,
        ]
      );
    }

    $sessionKey = uuidv4();
    executeNonQuery(
      'INSERT INTO session (user_id, session_key, created_at)
      VALUES (:user_id, :session_key, :created_at);',
      [
        'user_id' => $user['userId'],
        'session_key' => $sessionKey,
        'created_at' => getIsoDate('now'),
      ]
    );

    setSession($sessionKey);
    output([
      'status' => 'success',
      'userId' => $user['userId'],
      'sessionKey' => $sessionKey,
    ]);
  },

  'logout' => function () {
    $sessionKey = getSessionKey();

    $rowCount = executeNonQuery(
      'DELETE FROM session
      WHERE session_key = :session_key',
      ['session_key' => $sessionKey]
    );

    setSession(null);
    output([
      'status' => 'success',
      'message' =>
        $rowCount === 1
        ? 'Logged out successfully'
        : 'Logout not needed, because the session key is invalid',
    ]);
  },

  'logOutOtherSessions' => function () {
    $sessionKey = getSessionKey();

    $rowCount = executeNonQuery(
      'DELETE FROM session
        JOIN session AS current_session
          ON session.user_id = current_session.user_id
      WHERE
        current_session.session_key = :session_key
        AND session.session_key != :session_key',
      ['session_key' => $sessionKey]
    );

    output([
      'status' => 'success',
      'message' => "Logged out $rowCount other session(s)"
    ]);
  },

  'getMe' => function () {
    $sessionKey = getSessionKey();
    if ($sessionKey) {
      $me = fetchSingleRow(
        'SELECT
          user.user_id AS userId,
          user.username,
          user.created_at AS createdAt
        FROM user
          JOIN session
            ON user.user_id = session.user_id
        WHERE session.session_key = :session_key',
        ['session_key' => $sessionKey]
      );

      if ($me) {
        $me['isLoggedIn'] = true;
        output($me);
      } else {
        output([
          'isLoggedIn' => false,
          'reason' => 'old-session',
          'sessionKey' => $sessionKey,
        ]);
      }
    } else {
      output(['isLoggedIn' => false]);
    }
  },

  'createReceipt' => function () {
    $userId = getSessionUserId();
    $body = getBody();
    empty ($body['receiptDate']) && badRequest('Missing prop receiptDate');

    $receiptId = uuidv4();

    executeNonQuery(
      'INSERT INTO receipt (receipt_id, user_id, created_at, receipt_date)
      VALUES (:receipt_id, :user_id, :created_at, :receipt_date);',
      [
        'receipt_id' => $receiptId,
        'user_id' => $userId,
        'created_at' => getIsoDate('now'),
        'receipt_date' => $body['receiptDate'],
      ]
    );

    output(['status' => 'success', 'receiptId' => $receiptId]);
  },

  'updateReceipt' => function () {
    $userId = getSessionUserId();
    $db = getDb();
    $body = getBody();
    $config = getConfig();

    isset ($body['receiptId']) || badRequest('Missing prop receiptId');
    isset ($body['receiptDate']) || badRequest('Missing prop receiptDate');
    isset ($body['description']) || badRequest('Missing prop description');
    // locationCoords is nullable.
    isset ($body['locationText']) || badRequest('Missing prop locationText');
    isset ($body['files']) || badRequest('Missing prop files');

    foreach ($body['files'] as $file) {
      isset ($file['action']) || badRequest('Missing prop files.action');
      if ($file['action'] === 'create') {
        isAllowedDataUrl($file['dataUrl'], $config->maxAllowedFileSize)
          || badRequest("File must be a JPEG image and less than $config->maxAllowedFileSize bytes.");
      }
    }

    try {
      $db->beginTransaction();

      $receiptMatchesUser = fetchSingleRow(
        'SELECT 1
        FROM receipt
        WHERE
          receipt.user_id = :user_id
          AND receipt.receipt_id = :receipt_id',
        [
          'user_id' => $userId,
          'receipt_id' => $body['receiptId'],
        ]
      );
      if (!$receiptMatchesUser) {
        unauthorized('Receipt not available to logged in user');
      }

      executeNonQuery(
        'UPDATE receipt
        SET
          receipt_date = :receipt_date,
          description = :description,
          location_coords = :location_coords,
          location_text = :location_text
        WHERE receipt_id = :receipt_id',
        [
          'receipt_id' => $body['receiptId'],
          'receipt_date' => $body['receiptDate'],
          'description' => $body['description'],
          'location_coords' => isset ($body['locationCoords'])
            ? implode(',', $body['locationCoords'])
            : null,
          'location_text' => $body['locationText']
        ]
      );

      $existingFileIds = fetchAllRows(
        'SELECT file_id
        FROM receipt_file
        WHERE receipt_id = :receipt_id',
        ['receipt_id' => $body['receiptId']]
      );
      $existingFileIds = array_column($existingFileIds, 'file_id');
      $existingFileIdsHandled = [];

      // Handle files.
      foreach ($body['files'] as $file) {
        switch ($file['action']) {
          case 'create':
            $parsedData = parseDataUrl($file['dataUrl']);
            $fileHash = hash('sha256', $parsedData['data']);
            $fileDir = getFileDir($fileHash);
            if (!is_dir($fileDir)) {
              mkdir($fileDir, 0777, true);
            }
            file_put_contents("$fileDir/$fileHash.$parsedData[extension]", $parsedData['data']);
            $fileId = uuidv4();
            executeNonQuery(
              'INSERT INTO file (file_id, hash, extension, size, created_at)
              VALUES (:file_id, :hash, :extension, :size, :created_at);',
              [
                'file_id' => $fileId,
                'hash' => $fileHash,
                'extension' => $parsedData['extension'],
                'size' => strlen($parsedData['data']),
                'created_at' => getIsoDate('now'),
              ]
            );
            executeNonQuery(
              'INSERT INTO receipt_file (receipt_id, file_id)
              VALUES (:receipt_id, :file_id);',
              [
                'receipt_id' => $body['receiptId'],
                'file_id' => $fileId,
              ]
            );
            break;

          case 'keep':
          case 'delete':
            if (!in_array($file['fileId'], $existingFileIds, true)) {
              $db->rollBack();
              badRequest("File $file[fileId] doesn't exist, can't keep it");
            }

            if ($file['action'] === 'delete') {
              executeNonQuery(
                'DELETE FROM receipt_file
                WHERE receipt_id = :receipt_id AND file_id = :file_id',
                [
                  'receipt_id' => $body['receiptId'],
                  'file_id' => $file['fileId'],
                ]
              );
            }

            $existingFileIdsHandled[] = $file['fileId'];
            break;
        }
      }

      $unhandledExistingFiles = array_diff($existingFileIds, $existingFileIdsHandled);
      if (count($unhandledExistingFiles)) {
        $db->rollBack();
        badRequest('Files were not included in the request: ' . implode(', ', $unhandledExistingFiles));
      }

      $db->commit();
      output(['status' => 'success']);
    } catch (Exception $e) {
      $db->rollBack();
      internalServerError([
        'message' => 'Failed to update receipt: ' . $e->getMessage(),
        'exception' => (string) $e,
      ]);
    }
  },

  'deleteReceipt' => function () {
    $userId = getSessionUserId();
    $body = getBody();
    isset ($body['receiptId']) || badRequest('Missing prop receiptId');

    $receiptMatchesUser = fetchSingleRow(
      'SELECT 1
      FROM receipt
      WHERE
        user_id = :user_id
        AND receipt_id = :receipt_id',
      [
        'user_id' => $userId,
        'receipt_id' => $body['receiptId'],
      ]
    );
    if (!$receiptMatchesUser) {
      unauthorized('Receipt not available to logged in user');
    }

    executeNonQuery(
      "DELETE FROM receipt_file WHERE receipt_id = :receipt_id",
      ['receipt_id' => $body['receiptId']]
    );

    executeNonQuery(
      "DELETE FROM receipt WHERE receipt_id = :receipt_id",
      ['receipt_id' => $body['receiptId']]
    );

    output(['status' => 'success', 'message' => 'Receipt deleted successfully']);
  },

  'getReceipts' => function () {
    $userId = getSessionUserId();

    $receipts = fetchAllRows(
      "SELECT
        receipt.receipt_id AS receiptId,
        receipt.receipt_date AS receiptDate,
        receipt.description,
        receipt.location_coords AS locationCoords,
        receipt.location_text AS locationText,
        GROUP_CONCAT(
          file.file_id,
          ';'
          -- Only supported in SQLite 3.44, released on 2023-11-01.
          -- ORDER BY file.created_at
        ) AS files
      FROM receipt
        LEFT JOIN receipt_file
          ON receipt_file.receipt_id = receipt.receipt_id
        LEFT JOIN file
          ON file.file_id = receipt_file.file_id
      WHERE receipt.user_id = :user_id
      GROUP BY receipt.receipt_id;",
      ['user_id' => $userId]
    );

    foreach ($receipts as $key => $value) {
      if ($value['locationCoords']) {
        $value['locationCoords'] = explode(',', $value['locationCoords']);
      }

      $files = $value['files'];
      if ($files === null) {
        $files = [];
      } else {
        $files = explode(';', $files);
        $files = array_map(function ($fileId) {
          return [
            'fileId' => $fileId,
            'url' => getFileUrl($fileId),
          ];
        }, $files);
      }
      $receipts[$key]['files'] = $files;
    }

    output(['status' => 'success', 'receipts' => $receipts]);
  },

  'getFile' => function () {
    isset ($_GET['fileId']) || badRequest('Missing prop fileId');

    $file = fetchSingleRow(
      'SELECT hash, extension
      FROM file
      WHERE file_id = :file_id',
      ['file_id' => $_GET['fileId']]
    );

    if (!$file) {
      notFound();
    }

    $file = getFileDir($file['hash']) . "/$file[hash].$file[extension]";

    header('content-type: image/jpeg');
    header('cache-control: private, max-age=31536000, stale-while-revalidate=604800');
    readfile($file);
  },

  'getVersion' => function () {
    $config = getConfig();
    output(['version' => $config->version]);
  },
];

if ($isMainScript) {
  runApi($actions);
}
