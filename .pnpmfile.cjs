// @ts-check

/**
 * @typedef {{
 *   name: string;
 *   version: string;
 *   peerDependencies?: Record<string, string>;
 * }} PackageJson
 *
 * @param {PackageJson} pkg
 */
function readPackage(pkg) {
  // Patch support for React 19.
  if (pkg.name === "@reduxjs/toolkit" && pkg.peerDependencies) {
    pkg.peerDependencies.react = "*";
  }

  if (pkg.name === "react-redux" && pkg.peerDependencies) {
    pkg.peerDependencies.react = "*";
    pkg.peerDependencies["@types/react"] = "*";
  }

  if (pkg.name === "use-sync-external-store" && pkg.peerDependencies) {
    pkg.peerDependencies.react = "*";
  }

  // Patch support for ESLint 9.
  if (pkg.name === "eslint-plugin-react" && pkg.peerDependencies) {
    pkg.peerDependencies.eslint = "*";
  }

  if (pkg.name === "eslint-plugin-react-hooks" && pkg.peerDependencies) {
    pkg.peerDependencies.eslint = "*";
  }

  return pkg;
}

module.exports = {
  hooks: {
    readPackage,
  },
};
