import type { Receipt } from "./types";

export const isDraftReceipt = ({ description, locationText, files }: Receipt) =>
  !description && !locationText && !files.length;
