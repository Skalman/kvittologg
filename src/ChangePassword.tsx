import type React from "react";
import { useState } from "react";

import { ErrorAlert } from "./ErrorAlert";
import { FormControl } from "./FormControl";
import { Loading } from "./Loading";
import { useChangePasswordMutation } from "./redux/reducers";

export const ChangePassword: React.FC = () => {
  const [changePassword, { isLoading, error, isSuccess, reset }] =
    useChangePasswordMutation();
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [newPassword2, setNewPassword2] = useState("");
  const [hasPasswordMismatch, setHasPasswordMismatch] = useState(false);

  const handleSubmit = async () => {
    reset();
    const mismatch = newPassword !== newPassword2;
    setHasPasswordMismatch(mismatch);
    if (!mismatch) {
      const { error } = await changePassword({
        oldPassword,
        password: newPassword,
      });

      if (!error) {
        setOldPassword("");
        setNewPassword("");
        setNewPassword2("");
      }
    }
  };

  return (
    <form
      className="flex flex-col gap-4"
      onSubmit={(e) => {
        e.preventDefault();
        void handleSubmit();
      }}
    >
      <div>
        <FormControl
          label="Gammalt lösenord"
          type="password"
          value={oldPassword}
          onChange={(e) => {
            setOldPassword(e.target.value);
          }}
        />
        <FormControl
          label="Nytt lösenord"
          type="password"
          value={newPassword}
          onChange={(e) => {
            setNewPassword(e.target.value);
            setHasPasswordMismatch(false);
          }}
        />
        <FormControl
          label="Upprepa nytt lösenord"
          type="password"
          error={hasPasswordMismatch}
          value={newPassword2}
          onChange={(e) => {
            setNewPassword2(e.target.value);
            setHasPasswordMismatch(false);
          }}
        />
      </div>
      {hasPasswordMismatch ?
        <ErrorAlert error="Lösenorden matchar inte" />
      : error ?
        <ErrorAlert error={error} />
      : false}
      {isSuccess && (
        <div className="alert alert-success">Lösenordet har ändrats.</div>
      )}
      <div>
        <button type="submit" className="btn btn-primary">
          Ändra
          {isLoading && (
            <>
              {" "}
              <Loading />
            </>
          )}
        </button>
      </div>
    </form>
  );
};
