// From https://stackoverflow.com/a/66046176

export async function bufferToBase64(buffer: ArrayBuffer | Uint8Array) {
  // Use a FileReader to generate a base64 data URI.
  const base64Url = await new Promise<string>((resolve) => {
    const reader = new FileReader();
    reader.onload = () => {
      resolve(reader.result as string);
    };
    reader.readAsDataURL(new Blob([buffer]));
  });

  return base64Url.slice(base64Url.indexOf(",") + 1);
}
