import type React from "react";

export const Checkbox: React.FC<{
  label: string;
  checked: boolean;
  onChange: (checked: boolean) => void;
  className?: string;
}> = ({ label, checked, onChange, className }) => {
  return (
    <div className={`form-control ${className ?? ""}`}>
      <label className="label cursor-pointer justify-start gap-2">
        <input
          type="checkbox"
          checked={checked}
          className="checkbox"
          onChange={(e) => {
            onChange(e.target.checked);
          }}
        />
        <span className="label-text">{label}</span>
      </label>
    </div>
  );
};
