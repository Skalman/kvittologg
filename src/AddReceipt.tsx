import type React from "react";
import { Temporal } from "temporal-polyfill";

import { ErrorAlert } from "./ErrorAlert";
import { PlusIcon } from "./icons";
import { Loading } from "./Loading";
import { useCreateReceiptMutation } from "./redux/reducers";

export const AddReceipt: React.FC = () => {
  const [createReceipt, { isLoading, error }] = useCreateReceiptMutation();

  return (
    <button
      className="card min-h-64 w-72 animate-appear bg-base-200 shadow-xl ring ring-inset ring-primary"
      type="button"
      disabled={isLoading}
      onClick={() => {
        void createReceipt({
          receiptDate: Temporal.Now.plainDateISO().toString(),
        });
      }}
    >
      <div className="card-body">
        <div className="card-title">
          <PlusIcon /> Nytt kvitto
          {isLoading && <Loading />}
        </div>
        <ErrorAlert error={error} />
      </div>
      <figure>
        <div
          className="h-24 grow bg-cover bg-center bg-no-repeat"
          style={{
            backgroundImage: `url(${document.baseURI}receipt.svg)`,
          }}
        />
      </figure>
    </button>
  );
};
