export interface Receipt {
  receiptId: string;
  receiptDate: string;
  description: string;
  locationCoords?: [number, number];
  locationText: string;
  files: ReceiptFile[];
}

export interface ReceiptFile {
  fileId: string;
  url: string;
}

export type UpdateReceiptFile =
  | { action: "create"; dataUrl: string }
  | { action: "keep" | "delete"; fileId: string };
