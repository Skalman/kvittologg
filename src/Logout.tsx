import type React from "react";

import { ErrorAlert } from "./ErrorAlert";
import { Loading } from "./Loading";
import { useLogoutMutation } from "./redux/reducers";

export interface LogoutProps {
  onLogout: () => void;
}

export const Logout: React.FC<LogoutProps> = ({ onLogout }) => {
  const [logOut, { isLoading, error }] = useLogoutMutation();

  const handleClick = async () => {
    const { data } = await logOut(undefined);
    if (data) {
      onLogout();
    }
  };

  return (
    <div>
      <button onClick={() => void handleClick()} type="button" className="btn">
        Logga ut
        {isLoading && (
          <>
            {" "}
            <Loading />
          </>
        )}
      </button>
      {error && <ErrorAlert error={error} />}
    </div>
  );
};
