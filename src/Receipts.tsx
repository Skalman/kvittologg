import type React from "react";

import { AddReceipt } from "./AddReceipt";
import { isDraftReceipt } from "./isDraftReceipt";
import { ReceiptCard } from "./ReceiptCard";
import { useGetReceiptsQuery } from "./redux/reducers";
import { sortBy } from "./sortBy";

export const Receipts: React.FC = () => {
  const { data, isLoading, error, refetch } = useGetReceiptsQuery(undefined, {
    pollingInterval: 1000 * 60 * 10,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    skipPollingIfUnfocused: true,
  });

  const receipts = data?.toSorted(
    sortBy(
      ["asc", (x) => (isDraftReceipt(x) ? 1 : 2)],
      ["desc", (x) => x.receiptDate],
    ),
  );

  const hasDraft = receipts?.[0] !== undefined && isDraftReceipt(receipts[0]);

  return (
    <div className="flex flex-col gap-8">
      {error && (
        <p className="alert alert-error">
          Kunde inte ladda kvitton.{" "}
          <button
            type="button"
            className="btn btn-primary"
            onClick={() => void refetch()}
          >
            Försök igen
          </button>
        </p>
      )}

      <div className="flex flex-wrap gap-8">
        {isLoading ?
          <>
            <div className="skeleton min-h-64 w-72" />
            <div className="skeleton min-h-64 w-72" />
            <div className="skeleton min-h-64 w-72" />
          </>
        : <>
            {!hasDraft && <AddReceipt />}
            {receipts?.map((receipt) => (
              <ReceiptCard key={receipt.receiptId} receipt={receipt} />
            ))}
          </>
        }
      </div>
    </div>
  );
};
