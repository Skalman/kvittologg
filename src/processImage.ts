export const processImage = async ({
  file,
  maxSize,
  jpegQuality,
  signal,
}: {
  file: File;
  maxSize?: number;
  jpegQuality?: number;
  signal?: AbortSignal;
}): Promise<string> => {
  if (!maxSize && !jpegQuality) {
    return URL.createObjectURL(file);
  }

  return new Promise<string>((resolve, reject) => {
    if (signal?.aborted) {
      reject(signal.reason as Error);
      return;
    }
    const reader = new FileReader();

    signal?.addEventListener("abort", () => {
      reader.abort();
      reject(signal.reason as Error);
    });

    reader.onloadend = () => {
      const img = new Image();
      img.src = reader.result as string;

      img.onload = () => {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");
        let width = img.width;
        let height = img.height;

        if (maxSize) {
          if (width > maxSize || height > maxSize) {
            if (width < height) {
              width *= maxSize / height;
              height = maxSize;
            } else {
              height *= maxSize / width;
              width = maxSize;
            }
          }
        }

        canvas.width = width;
        canvas.height = height;
        ctx?.drawImage(img, 0, 0, width, height);
        const dataUrl = canvas.toDataURL("image/jpeg", jpegQuality);
        resolve(dataUrl);
      };
    };

    reader.readAsDataURL(file);
  });
};
