import type React from "react";
import { useEffect, useRef, useState } from "react";

import { CrossIcon, PlusIcon } from "./icons";
import { processImage } from "./processImage";

export type ReceiptFileInputValue =
  | {
      action: "create" | "deleteNew";
      dataUrl: string;
      fileId?: undefined;
      url?: undefined;
    }
  | {
      action: "keep" | "delete";
      dataUrl?: undefined;
      fileId: string;
      url: string;
    };

export interface ReceiptFilesInputProps {
  value: ReceiptFileInputValue[];
  onChange: (newValue: ReceiptFileInputValue[]) => void;
}

export const ReceiptFilesInput: React.FC<ReceiptFilesInputProps> = ({
  value,
  onChange,
}) => {
  const processAbortController = useRef<AbortController | undefined>(undefined);

  const [selectedFile, setSelectedFile] = useState<number>();

  useEffect(() => {
    return () => {
      processAbortController.current?.abort();
    };
  }, []);

  const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) {
      return;
    }

    processAbortController.current?.abort();
    processAbortController.current = new AbortController();

    const dataUrl = await processImage({
      file,
      maxSize: 1200,
      jpegQuality: 0.8,
      signal: processAbortController.current.signal,
    });

    onChange([...value, { action: "create", dataUrl }]);
  };

  return (
    <div className="flex flex-wrap gap-4">
      {value.map(({ action, dataUrl, fileId, url }, index) => {
        return (
          <div key={fileId ?? index} className="relative h-48 max-w-48">
            {(action === "create" || action === "deleteNew") && (
              <div className="badge badge-primary absolute left-4 top-4 shadow">
                Ny
              </div>
            )}
            {(action === "create" || action === "keep") && (
              <>
                <button
                  type="button"
                  className="absolute size-full rounded-xl"
                  onClick={() => {
                    setSelectedFile(index);
                  }}
                ></button>
                <button
                  type="button"
                  className="btn btn-square btn-error btn-sm absolute right-2 top-2"
                  onClick={() => {
                    const newValue = value.slice();
                    if (action === "create") {
                      newValue[index] = { action: "deleteNew", dataUrl };
                      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
                    } else if (action === "keep") {
                      newValue[index] = { action: "delete", fileId, url };
                    }
                    onChange(newValue);
                  }}
                >
                  <CrossIcon />
                </button>
                {selectedFile === index && (
                  <dialog
                    ref={(elem) => {
                      if (!elem) return;

                      elem.showModal();
                      elem.onclose = () => {
                        setSelectedFile(undefined);
                      };
                    }}
                    className="modal"
                  >
                    <form method="dialog">
                      <button
                        type="submit"
                        className="flex max-h-svh min-h-60 min-w-60 max-w-[100svw] items-center justify-center rounded-xl bg-neutral"
                      >
                        <img
                          src={dataUrl ?? url}
                          className="max-h-[90svh] max-w-[90svw] rounded-xl"
                        />
                      </button>
                    </form>
                  </dialog>
                )}
              </>
            )}
            {(action === "delete" || action === "deleteNew") && (
              <div className="absolute flex size-full flex-col items-center justify-center gap-4 bg-base-100/70">
                <button
                  type="button"
                  className="btn shadow"
                  onClick={() => {
                    const newValue = value.slice();
                    if (action === "deleteNew") {
                      newValue[index] = { action: "create", dataUrl };
                      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
                    } else if (action === "delete") {
                      newValue[index] = { action: "keep", fileId, url };
                    }
                    onChange(newValue);
                  }}
                >
                  Ångra
                </button>
              </div>
            )}
            <img
              src={dataUrl ?? url}
              className="h-full rounded-xl object-cover"
            />
          </div>
        );
      })}
      <label className="card cursor-pointer outline-none ring ring-inset ring-primary focus-within:outline-primary">
        <div className="card-body">
          <div>
            <input
              type="file"
              accept="image/*;capture=camera"
              className="sr-only"
              onChange={(e) => void handleFileChange(e)}
            />
            <PlusIcon /> Nytt foto
          </div>
        </div>
      </label>
    </div>
  );
};
