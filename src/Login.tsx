import type React from "react";
import { useState } from "react";

import { CrossCircleIcon, KeyIcon, UserIcon } from "./icons";
import { useCreateUserMutation, useLoginMutation } from "./redux/reducers";

export const Login: React.FC = () => {
  const [mode, setMode] = useState<"login" | "createAccount">("login");
  const [login, loginStatus] = useLoginMutation();
  const [createAccount, createAccountStatus] = useCreateUserMutation();

  const submit = mode === "login" ? login : createAccount;
  const { isError, isLoading, error } =
    mode === "login" ? loginStatus : createAccountStatus;

  return (
    <dialog
      className="modal"
      ref={(elem) => {
        elem?.showModal();
      }}
    >
      <div className="modal-box">
        <form
          className="flex flex-col gap-4"
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          action={async (formData) => {
            if (!isLoading) {
              const username = formData.get("username") as string;
              const password = formData.get("password") as string;
              await submit({ username, password });
            }
          }}
        >
          <h3 className="text-lg font-bold">
            {mode === "login" ? "Logga in" : "Skapa konto"}
          </h3>
          <label className="input input-bordered flex items-center gap-2">
            <UserIcon />
            <input
              type="text"
              className="grow"
              placeholder="Användarnamn"
              name="username"
              // value={username}
              // onChange={(e) => {
              //   setUsername(e.target.value);
              // }}
            />
          </label>
          <label className="input input-bordered flex items-center gap-2">
            <KeyIcon />
            <input
              type="password"
              className="grow"
              placeholder="Lösenord"
              name="password"
              // value={password}
              // onChange={(e) => {
              //   setPassword(e.target.value);
              // }}
            />
          </label>
          {isError && (
            <div role="alert" className="alert alert-error">
              <CrossCircleIcon />
              <span>{String(error)}</span>
            </div>
          )}
          <button type="submit" className="btn btn-primary">
            {mode === "login" ? "Logga in" : "Skapa konto"}
            {isLoading && <span className="loading loading-spinner"></span>}
          </button>
          {mode === "createAccount" && (
            <p>
              Har du redan ett konto?{" "}
              <button
                type="button"
                className="link"
                onClick={() => {
                  setMode("login");
                }}
              >
                Logga in
              </button>
            </p>
          )}
          {mode === "login" && (
            <p>
              Inget konto?{" "}
              <button
                type="button"
                className="link"
                onClick={() => {
                  setMode("createAccount");
                }}
              >
                Skapa ett
              </button>
            </p>
          )}
        </form>
      </div>
    </dialog>
  );
};
