import type React from "react";
import { useEffect, useId } from "react";
import { useDispatch, useSelector } from "react-redux";

import { UserIcon } from "./icons";
import { Login } from "./Login";
import { NewVersionAlert } from "./NewVersionAlert";
import { PerformanceDetails } from "./PerformanceDetails";
import { Receipts } from "./Receipts";
import {
  toggleDrawer,
  useGetMeQuery,
  useGetVersionQuery,
} from "./redux/reducers";
import { selectIsDrawerOpen, selectNeedLogin } from "./redux/selectors";
import { Settings } from "./Settings";

export const App: React.FC = () => {
  const drawerId = useId();
  const dispatch = useDispatch();
  const needLogin = useSelector(selectNeedLogin);
  const isDrawerOpen = useSelector(selectIsDrawerOpen);

  useGetVersionQuery(undefined, {
    pollingInterval: 1000 * 60 * 10,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    skipPollingIfUnfocused: true,
  });

  const { data: me } = useGetMeQuery(undefined);

  useEffect(() => {
    if (!isDrawerOpen) {
      return;
    }

    const listener = (e: KeyboardEvent) => {
      if (e.key === "Escape") {
        dispatch(toggleDrawer());
      }
    };
    window.addEventListener("keydown", listener);
    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, [dispatch, isDrawerOpen]);

  return (
    <div className="drawer h-full overflow-auto">
      <input
        id={drawerId}
        type="checkbox"
        className="drawer-toggle"
        checked={isDrawerOpen}
        onChange={() => {
          dispatch(toggleDrawer());
        }}
      />
      {(needLogin || me?.isLoggedIn === false) && <Login />}

      <div className="drawer-content bg-base-200">
        <div className="mx-auto max-w-screen-xl px-4 pb-20 pt-4">
          <div className="flex gap-2 p-4">
            {me?.username && (
              <button
                className={`btn btn-ghost btn-sm ${isDrawerOpen ? "btn-outline" : ""}`}
                onClick={() => dispatch(toggleDrawer())}
              >
                <UserIcon />
                {me.username}
              </button>
            )}
          </div>
          <NewVersionAlert className="mx-auto max-w-lg p-4" />
          <PerformanceDetails />
          {me?.isLoggedIn === true && <Receipts />}
        </div>
      </div>
      <div className="drawer-side z-[2]">
        <label
          htmlFor={drawerId}
          aria-label="close sidebar"
          className="drawer-overlay"
        ></label>
        <div className="min-h-full w-80 max-w-full bg-base-200 p-4 text-base-content">
          {isDrawerOpen && (
            <Settings onClose={() => dispatch(toggleDrawer())} />
          )}
        </div>
      </div>
    </div>
  );
};
