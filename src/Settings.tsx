import type { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import type React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";

import { ChangePassword } from "./ChangePassword";
import { Checkbox } from "./Checkbox";
import { CrossIcon } from "./icons";
import { Logout } from "./Logout";
import { resetSettings, saveSettings } from "./redux/reducers";
import { selectSettings, useAppSelector } from "./redux/selectors";

interface SettingsProps {
  onClose: () => void;
}

type SaveSettingsPayload =
  typeof saveSettings extends ActionCreatorWithPayload<infer T> ? T : never;

export const Settings: React.FC<SettingsProps> = ({ onClose }) => {
  const dispatch = useDispatch();
  const {
    theme,
    zoom,
    disableAnimation,
    disableRoundedCorners,
    disableShadows,
    disableScrollStick,
    showPerformance,
  } = useAppSelector(selectSettings);

  const save = (payload: SaveSettingsPayload) => {
    dispatch(saveSettings(payload));
  };

  const [localZoom, setLocalZoom] = useState(zoom);
  const [isChangingZoom, setIsChangingZoom] = useState(false);

  return (
    <div>
      <div className="mb-2 flex items-center justify-between gap-4">
        <h2 className="text-xl">Inställningar</h2>
        <button
          type="button"
          className="btn btn-square btn-outline float-right"
          onClick={onClose}
          aria-label="Stäng"
        >
          <CrossIcon />
        </button>
      </div>

      <Logout
        onLogout={() => {
          onClose();
        }}
      />

      <div className="mb-2">
        <p>Tema</p>
        <select
          value={theme ?? "auto"}
          onChange={(e) => {
            const theme = e.target.value;
            save({
              theme: theme === "light" || theme === "dark" ? theme : undefined,
            });
          }}
          className="select select-bordered"
        >
          <option value="auto">Automatisk</option>
          <option value="light">Ljus</option>
          <option value="dark">Mörk</option>
        </select>
      </div>

      <p className="mb-2">Zoom</p>
      <div className="relative">
        {isChangingZoom && (
          <div
            className="animate-slidein transition-font-size pointer-events-none absolute bottom-0 mb-2 w-full rounded border bg-neutral p-4 text-neutral-content shadow"
            style={{ fontSize: `${String(localZoom ?? 16)}px` }}
          >
            Exempel på textstorlek
          </div>
        )}
      </div>
      <input
        type="range"
        min={10}
        max={24}
        value={localZoom ?? 16}
        onFocus={() => {
          setIsChangingZoom(true);
        }}
        onChange={(e) => {
          setLocalZoom(+e.target.value || undefined);
        }}
        onBlur={() => {
          setIsChangingZoom(false);
          save({ zoom: localZoom });
        }}
        onTouchStart={() => {
          setIsChangingZoom(true);
        }}
        onTouchEnd={() => {
          setIsChangingZoom(false);
          save({ zoom: localZoom });
        }}
        className="range"
        step={2}
      />

      <details className="collapse collapse-arrow">
        <summary className="collapse-title">Ändra lösenord</summary>
        <div className="collapse-content">
          <ChangePassword />
        </div>
      </details>

      <details className="collapse collapse-arrow">
        <summary className="collapse-title">Avancerat</summary>
        <div className="collapse-content">
          <Checkbox
            label="Avaktivera animationer"
            checked={disableAnimation ?? false}
            onChange={(checked) => {
              save({ disableAnimation: checked || undefined });
            }}
          />
          <Checkbox
            label="Avaktivera rundade hörn"
            checked={disableRoundedCorners ?? false}
            onChange={(checked) => {
              save({ disableRoundedCorners: checked || undefined });
            }}
          />
          <Checkbox
            label="Avaktivera skuggor"
            checked={disableShadows ?? false}
            onChange={(checked) => {
              save({ disableShadows: checked || undefined });
            }}
          />
          <Checkbox
            label="Avaktivera fixering av rubriker vid skroll"
            checked={disableScrollStick ?? false}
            onChange={(checked) => {
              save({ disableScrollStick: checked || undefined });
            }}
          />
          <Checkbox
            label="Visa prestandadetaljer"
            checked={showPerformance ?? false}
            onChange={(checked) => {
              save({ showPerformance: checked || undefined });
            }}
          />
        </div>
      </details>

      <div className="mt-4 flex gap-4">
        <button type="button" onClick={onClose} className="btn btn-primary">
          Spara
        </button>
        <button
          type="button"
          className="btn btn-outline"
          onClick={() => {
            dispatch(resetSettings());
            onClose();
          }}
        >
          Avbryt
        </button>
      </div>
    </div>
  );
};
