import React from "react";
import { useSelector } from "react-redux";

import { InfoIcon } from "./icons";
import { selectHasNewVersion } from "./redux/selectors";

export const NewVersionAlert: React.FC<{ className?: string }> = ({
  className,
}) => {
  const hasNewVersion = useSelector(selectHasNewVersion);
  if (!hasNewVersion) {
    return;
  }

  return (
    <div className={className}>
      <div className="alert alert-info">
        <span />
        <span>
          <InfoIcon /> Ny uppdatering tillgänglig.
        </span>
        <a
          href=""
          className="btn btn-primary btn-sm"
          onClick={(e) => {
            e.preventDefault();
            location.reload();
          }}
        >
          Ladda om
        </a>
      </div>
    </div>
  );
};
