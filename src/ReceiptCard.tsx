import type React from "react";
import { useState } from "react";

import { EditReceipt } from "./EditReceipt";
import { EditIcon } from "./icons";
import { isDraftReceipt } from "./isDraftReceipt";
import { isTruthy } from "./isTruthy";
import { useDeleteReceiptMutation } from "./redux/reducers";
import type { Receipt } from "./types";

export interface ReceiptCardProps {
  receipt: Receipt;
}

export const ReceiptCard: React.FC<ReceiptCardProps> = ({ receipt }) => {
  const isDraft = isDraftReceipt(receipt);
  const [edit, setEdit] = useState(isDraft);
  const [deleteReceipt] = useDeleteReceiptMutation();

  const { receiptId, receiptDate, description, locationText, files } = receipt;
  const info = [description, locationText].filter(isTruthy);
  const title = isDraft ? <i>Utkast</i> : info[0];
  const badges = [...info.slice(1), receiptDate];

  return (
    <>
      <div className="card w-72 bg-base-200 shadow-xl">
        <div className="card-body">
          <h2 className="card-title">{title}</h2>
          {badges.map((x) => (
            <div key={x} className="badge badge-outline">
              {x}
            </div>
          ))}
          <div className="card-actions">
            {isDraft ?
              <>
                <button
                  className="btn btn-primary btn-sm"
                  onClick={() => {
                    setEdit(true);
                  }}
                >
                  <EditIcon />
                  Ändra
                </button>
                <button
                  className="btn btn-outline btn-error btn-sm"
                  onClick={() => void deleteReceipt({ receiptId })}
                >
                  Radera
                </button>
              </>
            : <button
                className="btn btn-outline btn-primary btn-sm"
                onClick={() => {
                  setEdit(true);
                }}
              >
                <EditIcon />
                Ändra
              </button>
            }
          </div>
        </div>
        <figure>
          <div
            className="h-24 grow bg-cover bg-center bg-no-repeat"
            style={{
              backgroundImage: `url(${files[0]?.url ?? `${document.baseURI}receipt.svg`})`,
            }}
          />
        </figure>
      </div>
      {edit && (
        <EditReceipt
          receipt={receipt}
          onClose={() => {
            console.log("close edit receipt");
            setEdit(false);
          }}
        />
      )}
    </>
  );
};
