export type SortOrderPart<T> = [
  order: "asc" | "desc",
  fn: (value: T) => number | string,
];

export function sortBy<T>(
  ...parts: SortOrderPart<T>[]
): (a: T, b: T) => number {
  return (a: T, b: T) => {
    for (const [order, fn] of parts) {
      const keyA = fn(a);
      const keyB = fn(b);
      if (keyA !== keyB) {
        if (order === "asc") {
          return keyA < keyB ? -1 : 1;
        } else {
          return keyA < keyB ? 1 : -1;
        }
      }
    }

    return 0;
  };
}
