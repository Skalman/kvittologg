import type React from "react";
import { useId, useMemo, useState } from "react";

import { DeleteReceipt } from "./DeleteReceipt";
import { ErrorAlert } from "./ErrorAlert";
import { FormControl } from "./FormControl";
import { CrossIcon } from "./icons";
import { isTruthy } from "./isTruthy";
import { Loading } from "./Loading";
import {
  type ReceiptFileInputValue,
  ReceiptFilesInput,
} from "./ReceiptFilesInput";
import { useUpdateReceiptMutation } from "./redux/reducers";
import type { Receipt, UpdateReceiptFile } from "./types";

export interface EditReceiptProps {
  receipt: Receipt;
  onClose: () => void;
}

export const EditReceipt: React.FC<EditReceiptProps> = ({
  receipt,
  onClose,
}) => {
  const formId = useId();
  const [dialog, setDialog] = useState<HTMLDialogElement>();
  const [updateReceipt, { isLoading, error }] = useUpdateReceiptMutation();
  const initialFiles = useMemo(
    () =>
      receipt.files.map<ReceiptFileInputValue>(({ fileId, url }) => ({
        action: "keep",
        fileId,
        url,
      })),
    [receipt.files],
  );
  const [files, setFiles] = useState(initialFiles);

  const handleFormSubmit = async (formData: FormData) => {
    const { data } = await updateReceipt({
      receiptId: receipt.receiptId,
      receiptDate: formData.get("receiptDate") as string,
      description: formData.get("description") as string,
      locationText: formData.get("locationText") as string,
      files: files
        .map<UpdateReceiptFile | undefined>(({ action, dataUrl, fileId }) => {
          if (action === "create") {
            return { action, dataUrl };
          }

          if (action === "keep" || action === "delete") {
            return { action, fileId };
          }

          // Assert.
          action satisfies "deleteNew";
        })
        .filter(isTruthy),
    });

    if (data) {
      dialog?.close();
    }
  };

  return (
    <dialog
      className="modal"
      ref={(element) => {
        setDialog(element ?? undefined);
        if (!element) return;

        element.showModal();
        element.onclose = onClose;
      }}
    >
      <form
        id={formId}
        action={(formData) => void handleFormSubmit(formData)}
        className="modal-box"
      >
        <button
          type="button"
          className="btn btn-square btn-sm float-right"
          onClick={() => {
            dialog?.close();
          }}
        >
          <CrossIcon />
        </button>
        <h3 className="text-lg font-bold">Ändra kvitto</h3>

        <div className="flex flex-col gap-4">
          <div>
            <FormControl
              defaultValue={receipt.description}
              label="Beskrivning"
              name="description"
            />
            <FormControl
              defaultValue={receipt.locationText}
              label="Plats"
              name="locationText"
            />
            <FormControl
              defaultValue={receipt.receiptDate.toString()}
              label="Datum"
              name="receiptDate"
              type="date"
            />
          </div>
          <div>
            <ReceiptFilesInput
              value={files}
              onChange={(x) => {
                setFiles(x);
              }}
            />
          </div>
          {error && <ErrorAlert error={error} />}
        </div>
        <div className="modal-action">
          <button
            className="btn btn-primary"
            form={formId}
            disabled={isLoading}
          >
            Spara
            {isLoading && (
              <>
                {" "}
                <Loading />
              </>
            )}
          </button>
          <button
            className="btn"
            type="button"
            onClick={() => {
              dialog?.close();
            }}
          >
            Avbryt
          </button>
        </div>
        <div className="text-end">
          <DeleteReceipt receiptId={receipt.receiptId} />
        </div>
      </form>
    </dialog>
  );
};
