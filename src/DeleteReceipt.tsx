import type React from "react";
import { useEffect, useState } from "react";

import { useDeleteReceiptMutation } from "./redux/reducers";

export interface DeleteReceiptProps {
  receiptId: string;
}

export const DeleteReceipt: React.FC<DeleteReceiptProps> = ({ receiptId }) => {
  const [deleteReceipt, { isLoading }] = useDeleteReceiptMutation();
  const [state, setState] = useState<"idle" | "confirm">("idle");

  useEffect(() => {
    if (state === "confirm" && !isLoading) {
      const timeout = setTimeout(() => {
        setState("idle");
      }, 3000);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [isLoading, state]);

  if (state === "idle") {
    return (
      <button
        type="button"
        className="link"
        onClick={() => {
          setState("confirm");
        }}
      >
        Radera
      </button>
    );
  }

  return (
    <button
      type="button"
      className="link link-error"
      onClick={() => {
        void deleteReceipt({ receiptId });
      }}
    >
      Radera?
    </button>
  );
};
