import type React from "react";

export const ErrorAlert: React.FC<{ error: unknown }> = ({ error }) => {
  if (!error) {
    return;
  }

  return (
    <details className="collapse collapse-arrow bg-error text-error-content">
      <summary className="collapse-title font-medium">Fel</summary>
      <div className="collapse-content">{JSON.stringify(error)}</div>
    </details>
  );
};
