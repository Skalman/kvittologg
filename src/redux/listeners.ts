import { createListenerMiddleware } from "@reduxjs/toolkit/react";

import { settingsPersistence } from "./persistence";
import { reduxInit, type RootState } from "./reducers";
import { selectSettings } from "./selectors";

export const listenerMiddleware = createListenerMiddleware<RootState>();

// Settings persistence.
listenerMiddleware.startListening({
  predicate: (_action, currentState, previousState) => {
    return settingsPersistence.shouldPersist(currentState, previousState);
  },
  effect: (_action, listenerApi) => {
    settingsPersistence.persist(listenerApi.getState());
  },
});

// Settings that should change the root element.
listenerMiddleware.startListening({
  predicate: (action, currentState, previousState) => {
    return (
      action.type === reduxInit.type ||
      selectSettings(currentState) != selectSettings(previousState)
    );
  },
  effect: (_action, listenerApi) => {
    const {
      zoom,
      theme,
      disableAnimation,
      disableRoundedCorners,
      disableShadows,
      disableScrollStick,
    } = selectSettings(listenerApi.getState());
    const root = document.documentElement;
    const dataset = root.dataset;

    if (zoom) {
      root.style.fontSize = `${String(zoom)}px`;
    } else {
      root.style.fontSize = "";
    }

    if (theme) {
      dataset.theme = theme;
    } else {
      delete dataset.theme;
    }

    const setClass = (className: string, value: true | undefined) => {
      if (value) {
        root.classList.add(className);
      } else {
        root.classList.remove(className);
      }
    };

    setClass("disable-animation", disableAnimation);
    setClass("disable-rounded-corners", disableRoundedCorners);
    setClass("disable-shadows", disableShadows);
    setClass("disable-scroll-stick", disableScrollStick);
  },
});
