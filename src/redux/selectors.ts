import type { TypedUseSelectorHook } from "react-redux";
import { useSelector } from "react-redux";

import type { RootState } from "./reducers";

// const selectEventCache = (state: RootState) => state.eventCache;
// const selectEventUuid = (_state: RootState, uuid: string) => uuid;
// const selectUi = (state: RootState) => state.ui;

export const selectHasNewVersion = (state: RootState) =>
  state.app.hasNewVersion;
export const selectSettings = (state: RootState) => state.settings.current;
export const selectShowPerformance = (state: RootState) =>
  state.settings.current.showPerformance === true;

// export const selectName = (state: RootState) => state.settings.current.name;
// export const selectAllowEdit = (state: RootState) =>
//   state.settings.current.allowEdit === true &&
//   state.settings.current.name !== undefined;
// const selectShowOldEvents = (state: RootState) =>
//   state.settings.current.showOldEvents ?? false;

// export type TimeGroup = "past" | "thisWeek" | "nextWeek" | "future";
// const getTimeGroup = (dateTime: string): TimeGroup => {
//   const targetWeek = getStartOfWeek(new Date(dateTime));
//   const thisWeek = getStartOfWeek(new Date());
//   const nextWeek = new Date(thisWeek);
//   nextWeek.setDate(nextWeek.getDate() + 7);

//   return (
//     targetWeek < thisWeek ? "past"
//     : targetWeek.getTime() === thisWeek.getTime() ? "thisWeek"
//     : targetWeek.getTime() === nextWeek.getTime() ? "nextWeek"
//     : "future"
//   );
// };

// // `Map.groupBy` not currently supported in Safari.
// const mapGroupBy = <TItem, TKey>(
//   items: TItem[],
//   callbackFn: (item: TItem, index: number) => TKey,
// ) => {
//   return items.reduce<Map<TKey, TItem[]>>((map, item, index) => {
//     const key = callbackFn(item, index);
//     const items = map.get(key);
//     if (items) {
//       items.push(item);
//     } else {
//       map.set(key, [item]);
//     }
//     return map;
//   }, new Map());
// };

// export const selectEventUuidsByTimeGroup = createSelector(
//   [selectEventCache, selectShowOldEvents],
//   (eventCache, showOldEvents) => {
//     const today = getStartOfDay(new Date());
//     return new Map<TimeGroup, string[]>(
//       [
//         ...mapGroupBy(
//           [...Object.values(eventCache)]
//             .filter(isTruthy)
//             .filter(({ event }) => !event.isDeleted)
//             .filter(
//               ({ event }) =>
//                 showOldEvents || today <= new Date(event.startDateTime),
//             )
//             .sort((a, b) =>
//               a.event.startDateTime.localeCompare(b.event.startDateTime),
//             ),
//           (x) => getTimeGroup(x.event.startDateTime),
//         ).entries(),
//       ].map(
//         ([timeGroup, events]) =>
//           [timeGroup, events.map((x) => x.event.uuid)] as const,
//       ),
//     );
//   },
// );

// export const selectEvent = createSelector(
//   [selectEventCache, selectEventUuid],
//   (events, uuid) => events[uuid]?.event,
// );

// export const selectIsEventPending = createSelector(
//   [selectEventCache, selectEventUuid],
//   (events, uuid) => events[uuid]?.state === "pending",
// );

// export const selectScrollToEvent = createSelector(
//   selectUi,
//   selectEventUuid,
//   (ui, uuid) => ui.scrollToEventUuid === uuid,
// );

export const selectIsDrawerOpen = (state: RootState) => state.app.isDrawerOpen;
export const selectNeedLogin = (state: RootState) => state.app.needLogin;

// export const selectIsNewEventOpen = (state: RootState) =>
//   state.ui.isNewEventOpen;

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
