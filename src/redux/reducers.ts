import {
  createApi,
  type FetchArgs,
  fetchBaseQuery,
} from "@reduxjs/toolkit/query/react";
import {
  combineReducers,
  createSlice,
  type PayloadAction,
} from "@reduxjs/toolkit/react";

import type { Receipt, UpdateReceiptFile } from "../types";

interface AppInitialState {
  needLogin: boolean;
  version?: string;
  hasNewVersion: boolean;
  isDrawerOpen: boolean;
}

const appSlice = createSlice({
  name: "app",
  initialState: {
    needLogin: false,
    hasNewVersion: false,
    isDrawerOpen: false,
  } satisfies AppInitialState as AppInitialState,
  reducers: {
    reduxInit(state, { payload }: PayloadAction<{ version?: string }>) {
      state.version = payload.version;
    },
    needLogin(state) {
      state.needLogin = true;
    },
    loggedIn(state) {
      state.needLogin = false;
    },
    receivedVersion(state, { payload }: PayloadAction<string>) {
      state.hasNewVersion =
        state.version !== undefined && payload !== state.version;
    },
    toggleDrawer(state) {
      state.isDrawerOpen = !state.isDrawerOpen;
    },
  },
});

export const {
  actions: { reduxInit, needLogin, loggedIn, receivedVersion, toggleDrawer },
} = appSlice;

interface SettingsStateWrapper {
  current: SettingsState;
  original?: SettingsState;
}
export interface SettingsState {
  theme?: "light" | "dark";
  zoom?: number;
  disableAnimation?: true;
  disableRoundedCorners?: true;
  disableShadows?: true;
  disableScrollStick?: true;
  showPerformance?: true;
}
const settingsKeys: (keyof SettingsState)[] = [
  "theme",
  "zoom",
  "disableAnimation",
  "disableRoundedCorners",
  "disableShadows",
  "disableScrollStick",
  "showPerformance",
];
const settingsSlice = createSlice({
  name: "settings",
  initialState: { current: {} } as SettingsStateWrapper,
  reducers: {
    resetSettings(state) {
      if (state.original) {
        state.current = state.original;
        state.original = undefined;
      }
    },
    saveSettings(state, { payload }: PayloadAction<SettingsState>) {
      state.original ??= { ...state.current };
      for (const key of settingsKeys) {
        if (key in payload) {
          // @ts-expect-error TypeScript doesn't understand that it's the same key.
          state.current[key] = payload[key];
        }
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(toggleDrawer, (state) => {
      state.original = undefined;
    });
  },
});
export const {
  actions: { saveSettings, resetSettings },
} = settingsSlice;

const isLoginRequired = (body: unknown) => {
  return (
    typeof body === "object" &&
    body !== null &&
    "error" in body &&
    body.error == "login-required"
  );
};

const baseQuery = fetchBaseQuery({ baseUrl: "api.php" });

export const appApi = createApi({
  reducerPath: "appApi",
  tagTypes: ["UserData", "Receipt"],
  baseQuery: async (
    args: FetchArgs | Promise<FetchArgs>,
    api,
    extraOptions,
  ) => {
    args = await args;
    args.validateStatus = ({ status }, body) => {
      if (status === 401 && isLoginRequired(body)) {
        console.log("Response from server: login required");
        api.dispatch(needLogin());
      }

      return status >= 200 && status < 300;
    };

    return baseQuery(args, api, extraOptions);
  },

  endpoints: (builder) => ({
    createUser: builder.mutation<
      undefined,
      { username: string; password: string }
    >({
      query: (body) => ({
        url: "",
        params: { action: "createUser" },
        body,
        method: "POST",
      }),
      transformErrorResponse(response) {
        if (
          typeof response.data === "object" &&
          response.data &&
          "message" in response.data &&
          typeof response.data.message === "string"
        ) {
          return response.data.message;
        }
      },
    }),

    changePassword: builder.mutation<
      undefined,
      { password: string; oldPassword: string }
    >({
      query: (body) => ({
        url: "",
        params: { action: "changePassword" },
        body,
        method: "POST",
      }),
    }),

    login: builder.mutation<undefined, { username: string; password: string }>({
      query: (body) => ({
        url: "",
        params: { action: "login" },
        body,
        method: "POST",
      }),
      invalidatesTags: [{ type: "UserData" }],
      async onQueryStarted(_args, { dispatch, queryFulfilled }) {
        await queryFulfilled;
        dispatch(loggedIn());
      },
      transformErrorResponse(response) {
        if (
          typeof response.data === "object" &&
          response.data &&
          "message" in response.data &&
          typeof response.data.message === "string"
        ) {
          return response.data.message;
        }
      },
    }),

    logout: builder.mutation<true, undefined>({
      query: () => ({
        url: "",
        params: { action: "logout" },
        method: "POST",
      }),
      invalidatesTags: [{ type: "UserData" }],
      async onQueryStarted(_args, { dispatch, queryFulfilled }) {
        await queryFulfilled;
        dispatch(needLogin());
      },
      transformResponse: () => true,
    }),

    getMe: builder.query<{ isLoggedIn: boolean; username?: string }, undefined>(
      {
        query: () => ({ url: "", params: { action: "getMe" } }),
        providesTags: [{ type: "UserData" }],
      },
    ),

    createReceipt: builder.mutation<
      { receiptId: string },
      { receiptDate: string }
    >({
      query: (body) => ({
        url: "",
        params: { action: "createReceipt" },
        body,
        method: "POST",
      }),
      invalidatesTags: [{ type: "Receipt", id: "LIST" }],
    }),

    updateReceipt: builder.mutation<
      true,
      {
        receiptId: string;
        receiptDate: string;
        description: string;
        locationCoords?: [number, number];
        locationText: string;
        files: UpdateReceiptFile[];
      }
    >({
      query: ({
        receiptId,
        receiptDate,
        description,
        locationCoords,
        locationText,
        files,
      }) => ({
        url: "",
        params: { action: "updateReceipt" },
        body: {
          receiptId,
          receiptDate,
          description,
          locationCoords,
          locationText,
          files,
        },
        method: "POST",
      }),
      transformResponse() {
        return true;
      },
      invalidatesTags: (result, error, { receiptId }) =>
        error ? [] : [{ type: "Receipt", id: receiptId }],
    }),

    deleteReceipt: builder.mutation<undefined, { receiptId: string }>({
      query: (body) => ({
        url: "",
        params: { action: "deleteReceipt" },
        body,
        method: "DELETE",
      }),
      invalidatesTags: [{ type: "Receipt", id: "LIST" }],
    }),

    getReceipts: builder.query<Receipt[], undefined>({
      query: () => ({ url: "", params: { action: "getReceipts" } }),
      transformResponse(response: {
        receipts: {
          receiptId: string;
          receiptDate: string;
          description: string;
          locationCoords: [number, number] | null;
          locationText: string;
          files: { fileId: string; url: string }[];
        }[];
      }) {
        return response.receipts.map((x) => ({
          ...x,
          locationCoords: x.locationCoords ?? undefined,
        }));
      },
      providesTags: (result) =>
        result ?
          [
            ...result.map(
              ({ receiptId }) => ({ type: "Receipt", id: receiptId }) as const,
            ),
            { type: "Receipt", id: "LIST" },
            { type: "UserData" },
          ]
        : [{ type: "Receipt", id: "LIST" }, { type: "UserData" }],
    }),

    getVersion: builder.query<string, undefined>({
      query: () => ({ url: "", params: { action: "getVersion" } }),
      async onQueryStarted(_arg, { dispatch, queryFulfilled }) {
        const { data } = await queryFulfilled;
        dispatch(receivedVersion(data));
      },
      transformResponse({ version }: { version: string }) {
        return version;
      },
    }),
  }),
});

export const {
  useCreateUserMutation,
  useChangePasswordMutation,
  useLoginMutation,
  useLogoutMutation,
  useGetMeQuery,
  useCreateReceiptMutation,
  useUpdateReceiptMutation,
  useDeleteReceiptMutation,
  useGetReceiptsQuery,
  useGetVersionQuery,
} = appApi;

export const reducer = combineReducers({
  [appSlice.reducerPath]: appSlice.reducer,
  [appApi.reducerPath]: appApi.reducer,
  [settingsSlice.reducerPath]: settingsSlice.reducer,
});

export type RootState = ReturnType<typeof reducer>;
