import { setupListeners } from "@reduxjs/toolkit/query/react";
import { configureStore } from "@reduxjs/toolkit/react";

import { listenerMiddleware } from "./listeners";
import { settingsPersistence } from "./persistence";
import { appApi, reducer } from "./reducers";

let initialState = reducer(undefined, { type: "init" });
initialState = settingsPersistence.load(initialState);

export const store = configureStore({
  preloadedState: initialState,
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      appApi.middleware,
      listenerMiddleware.middleware,
    ),
});

setupListeners(store.dispatch);
