import type React from "react";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { selectShowPerformance } from "./redux/selectors";

export const PerformanceDetails: React.FC = () => {
  const showPerformance = useSelector(selectShowPerformance);

  const [fps, setFps] = useState<number>();

  useEffect(() => {
    if (!showPerformance) {
      return;
    }

    let fpsCounter = 0;
    let lastTime = 0;

    let handle = requestAnimationFrame(function calculateFPS() {
      const now = performance.now();
      const delta = now - lastTime;
      fpsCounter++;

      if (delta >= 1000) {
        const fps = fpsCounter * (1000 / delta);
        setFps(fps);
        fpsCounter = 0;
        lastTime = now;
      }

      handle = requestAnimationFrame(calculateFPS);
    });

    return () => {
      cancelAnimationFrame(handle);
    };
  }, [showPerformance]);

  if (!showPerformance || fps === undefined) {
    return null;
  }

  return (
    <div className="pointer-events-none fixed left-0 top-0 z-[3] border-b border-r bg-neutral px-2 text-neutral-content">
      {Math.round(fps * 10) / 10} FPS
    </div>
  );
};
