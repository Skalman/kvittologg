import type React from "react";

export const Loading: React.FC = () => (
  <span className="loading loading-spinner loading-sm"></span>
);
