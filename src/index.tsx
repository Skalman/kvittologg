import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";

import { App } from "./App";
import { reduxInit } from "./redux/reducers";
import { store } from "./redux/store";

const rootElem = document.createElement("div");
rootElem.id = "app";
document.body.append(rootElem);

store.dispatch(
  reduxInit({
    version: document.documentElement.dataset.version,
  }),
);

createRoot(rootElem).render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
);
