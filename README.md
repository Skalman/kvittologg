# Kvittologg

Kvittologg is a personal expense tracking system designed to help users manage
their receipts efficiently. It provides a simple interface for adding, updating,
and deleting receipts, along with viewing detailed reports. The system is built
using React, Redux, and Tailwind CSS for the frontend, and PHP with SQLite for
the backend.

## Features

- **Receipt Management**: Easily add, update, or delete receipts.
- **User Authentication**: Secure login and session management.
- **Responsive Design**: Works well on both desktop and mobile devices.
- **Dark Mode Support**: Toggle between light and dark themes.

## Getting Started

### Prerequisites

Ensure you have Node.js, PNPM, and PHP installed on your machine.

### Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/Skalman/kvittologg.git
cd kvittologg
```

2. Install JavaScript dependencies:

```bash
pnpm install
```

3. Copy `.env.example` to `.env` and optionally adjust the settings.

### Building the Project

Run the build script to compile the assets:

```bash
pnpm build
```

This command compiles the JavaScript and CSS assets into the `public/dist`
directory.

### Running the Project

Start a local server to serve the `public` directory:

```bash
php -S localhost:8000 -t public
```

Navigate to `http://localhost:8000` in your browser to view the application.

## Contributing

Contributions are welcome. Please feel free to fork the project, make your
changes, and submit a pull request.

## License

This project is licensed under the MPL-2.0 License. See the LICENSE file for
more details.
